# hosts
Thousands of domains that are tracking you!<br>
It has simple usage: just replace/merge your hosts file in your system with new one from this repo.<br><br>
It blocks:
* Windows 10 tracking
* Ads
* Malware sites
* Some sites that are just tracking you
* PRISM and NSA domains and IPs
<br><br>

There is 2 versions: <br>
Hard - Blocks accessing to socials, porn sites and other stuff you like, use only if you are completely paranoid<br>
Soft - Just blocks some tracking, PRISM and NSA, malware and some other stuff you probably don't want to use<br>
